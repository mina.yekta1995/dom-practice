const Header = ( () => {
    return createElement("header", {
        children:[
            createElement("button", {
                children : "CLICK ME :)"
            }),
            createElement("ul",{
                children: [
                    createElement("li",{
                        children: "Item1"
                    })
                ]
            })
        ]
    });
})();