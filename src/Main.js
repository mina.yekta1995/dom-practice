const Main = ( () => {
    return createElement("main", {
        children:[
            createElement("button", {
                children : "CLICK ME :)"
            }),
            createElement("ul",{
                children: [
                    createElement("li",{
                        children: "Item1"
                    })
                ]
            })
        ]
    });
})();