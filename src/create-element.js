/**
 * 
 * @param {string} tag 
 * @param {object} props 
 * @returns {HTMLElement}
 */
const createElement = ( tag , props = {}) => {
    console.log("xxxxxxxxx")
    const element = document.createElement(tag);
    //------------------validate props----------------------
    if( !props ){
        throw new Error("props not valid!");
    }
    const {children} = props;
    if( !children ){
        throw new Error("There is not any child");
    }
    //------------------Fill content------------------------
    if(Array.isArray(children)) {
        children.forEach((c => {
            element.appendChild(c);
        }))
    } else if ( typeof (children) === "string" ) {
        element.innerHtml = children;
    } else {
        throw new Error (" you passed child is not valid");
    }
    return element
}
